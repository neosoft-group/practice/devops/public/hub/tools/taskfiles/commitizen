# Behavior Tests

## Overview

The behavior tests in this project use the CodeceptJS testing framework to verify the functionality of the Commitizen versioning tool. These tests cover various aspects of the version bumping process, including:

1. **Automatic Version Increment**: Ensuring that the version is automatically incremented based on the type of commit (fix, feat, breaking change).
2. **Manual Version Bump**: Verifying that the version can be manually set using the `cz bump` command.
3. **Prerelease Version Support**: Checking the support for prerelease versions (alpha, beta, release candidate) during the version bumping process.
4. **Version Consistency Check**: Validating that the version is consistent across all version files before bumping the version.
5. **Installation Validation**: Confirming that the `install` task correctly sets up the Commitizen environment.

## Running the Tests

To run the behavior tests, follow these steps:

1. Ensure that you have Docker installed and configured on your system.
2. Navigate to the `tests/behavior` directory.
3. Run the following command to execute the tests:

   ```bash
   task test
   ```

   This will run all the behavior tests in parallel. If you want to run the tests in a non-parallel mode and focus on a specific tag, use the following command:

   ```bash
   PARALLEL=false task test -- --grep @tag
   ```

   Replace `@tag` with the desired tag (e.g., `@install`, `@bump-automatic-fix`, etc.) to run only the tests with that tag.

## Test Structure

The behavior tests are organized into the following feature files:

1. `install.feature`: Validates the `install` task functionality, ensuring that Commitizen and its dependencies are correctly installed.
2. `bump/automatic_version_increment.feature`: Tests the automatic version increment based on the type of commit (fix, feat, breaking change).
3. `bump/manual_version_bump.feature`: Verifies the manual version bump using the `cz bump` command.
4. `bump/prerelease_versions_support.feature`: Checks the support for prerelease versions (alpha, beta, release candidate) during the version bumping process.
5. `bump/version_consistency_check.feature`: Validates the version consistency check before bumping the version.

Each feature file contains one or more scenarios that describe the expected behavior of the system. The steps for these scenarios are defined in the `steps.js` file.

## Test Execution

The behavior tests are executed using the `task test` command, which runs the `behavior` task defined in the `Taskfile.yml`. This task sets up the necessary environment, installs dependencies, and runs the CodeceptJS tests.

The test execution can be customized by setting the following environment variables:

- `PARALLEL`: Set to `false` to run the tests in a non-parallel mode.
- `WORKERS`: Specify the number of worker processes to use for parallel test execution.
- `DEBUG`: Set to `true` to enable verbose logging during the test run.

## Troubleshooting

If you encounter any issues while running the behavior tests, please check the following:

1. Ensure that the Docker container is running and accessible.
2. Verify that the necessary dependencies are installed correctly.
3. Check the test output for any error messages or unexpected behavior.

If you cannot resolve the issue, please feel free to reach out for assistance.