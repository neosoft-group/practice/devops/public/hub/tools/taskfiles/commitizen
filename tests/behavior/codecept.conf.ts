import { setHeadlessWhen, setCommonPlugins } from '@codeceptjs/configure';
// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

// enable all common plugins https://github.com/codeceptjs/configure#setcommonplugins
setCommonPlugins();

export const config: CodeceptJS.MainConfig = {
  tests: './*_test.ts',
  output: './output',
  mocha: { bail: true },
  helpers: {    
    CommandHelper: {
      require: './helpers/CommandHelper.ts',
    },
  },
  gherkin: {
    features: './features/**/*.feature',
    steps: './step_definitions/**/*.js'
  },
  include: {
    I: './steps_file'
  },
  name: 'behavior'
}