@bump @bump-automatic
Feature: Automatically bump project version with cz bump command
  As a user
  I want cz bump to automatically increase the version based on commits
  So that the versioning reflects the changes made according to commit messages.

  @bump-automatic-fix
  Scenario: Bump patch version with a fix commit
    Given a container named "test-bump-fix" is prepared
    And there is a commit with "fix" in the message
    When I run command in container "~/.local/bin/cz bump --yes"
    Then the output should contain
      | content                   |
      | bump: version             |
      | →                         |
      | tag to create:            |
      | increment detected: PATCH |

  @bump-automatic-feat
  Scenario: Bump minor version with a feat commit
    Given a container named "test-bump-feat" is prepared
    And there is a commit with "feat" in the message
    When I run command in container "~/.local/bin/cz bump --yes"
    Then the output should contain
      | content                   |
      | bump: version             |
      | →                         |
      | tag to create:            |
      | increment detected: MINOR |

  @bump-automatic-breaking-change
  Scenario: Bump major version with a breaking change commit
    Given a container named "test-bump-breaking-change" is prepared
    And there is a commit with "feat!" in the message
    When I run command in container "~/.local/bin/cz bump --yes"
    Then the output should contain
      | content                   |
      | bump: version             |
      | →                         |
      | tag to create:            |
      | increment detected: MAJOR |
