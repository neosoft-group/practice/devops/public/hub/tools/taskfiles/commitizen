@bump @bump-manual
Feature: Manual version bump with cz bump
  As a user
  I want to manually specify the version using cz bump
  So that I can set the version directly when needed

  @bump-manual
  Scenario: Manually set version
    Given a container named "test-bump-manual" is prepared
    And there is a commit with "chore" in the message
    When I run command in container "~/.local/bin/cz bump --yes 2.0.0"
    Then the output should contain
      | content                   |
      | bump: version             |
      | → 2.0.0                   |
      | tag to create: 2.0.0      |
