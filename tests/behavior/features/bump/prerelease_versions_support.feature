@bump @bump-prerelease
Feature: Support for prerelease versions with cz bump
  As a user 
  I want to create prerelease versions with cz bump
  So that I can release alpha, beta, and release candidate versions according to semantic versioning.

  @bump-prerelease-alpha0
  Scenario: Bump version to alpha prerelease after feature addition
    Given a container named "test-bump-prerelease-alpha0" is prepared
    And there is a commit with "feat" in the message
    When I run command in container "~/.local/bin/cz bump --yes --prerelease alpha"
    Then the output should contain
      | content                   |
      | bump: version             |
      | →                         |
      | tag to create:            |
      | a0                        |
      | increment detected: MINOR |

  @bump-prerelease-alpha1
  Scenario: Bump version within alpha prerelease phase
    Given a container named "test-bump-prerelease-alpha1" is prepared
    And the current version is an alpha prerelease
    And there is a commit with "feat" in the message
    When I run command in container "~/.local/bin/cz bump --yes --prerelease alpha"
    Then the output should contain
      | content        |
      | bump: version  |
      | →              |
      | tag to create: |
      | a1             |

  @bump-prerelease-beta
  Scenario: Bump version to beta prerelease after feature addition
    Given a container named "test-bump-prerelease-beta" is prepared
    And there is a commit with "feat" in the message
    When I run command in container "~/.local/bin/cz bump --yes --prerelease beta"
    Then the output should contain 
      | content        |
      | bump: version  |
      | →              |
      | tag to create: |
      | b0             |

  @bump-prerelease-rc
  Scenario: Bump version to release candidate prerelease after feature addition
    Given a container named "test-bump-prerelease-rc" is prepared
    And there is a commit with "feat" in the message
    When I run command in container "~/.local/bin/cz bump --yes --prerelease rc"
    Then the output should contain 
      | content        |
      | bump: version  |
      | →              |
      | tag to create: |
      | rc0            |

  @bump-prerelease-dry-run-pass
  Scenario: Successfully perform a dry-run prerelease bump
    Given a container named "test-prerelease-dry-run-pass" is prepared
    And the container is on the "main" branch
    And there is a commit with "feat" in the message
    When I run command in container "task commitizen:bump:prerelease:dry-run"
    Then the output should contain
      | content                                                                              |
      | Running Commitizen dry-run prerelease bump on branch main with prerelease type rc... |
      | bump: version 1.0.0 → 1.1.0rc0                                                       |
      | tag to create: 1.1.0rc0                                                              |
      | increment detected: MINOR                                                            |

  @bump-prerelease-dry-run-fail @bump-prerelease-dry-run-fail-uncommitted-changes
  Scenario: Fail to perform a dry-run prerelease bump due to uncommitted changes
    Given a container named "test-bump-prerelease-dry-run-fail-uncommitted-changes" is prepared
    And the container is on the "main" branch
    And there are uncommitted changes in the container
    When I run command in container "task commitizen:bump:prerelease:dry-run" that should fail
    Then the output should contain
      | content                                                                             |
      | Uncommitted changes detected. Please commit or stash them before running this task. |
