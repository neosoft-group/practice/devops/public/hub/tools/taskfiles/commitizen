@bump @bump-consistency-check
Feature: Check version consistency before bumping
  As a user
  I want to check the consistency of the version across all version files
  Before bumping the version

  @bump-consistency-check-pass
  Scenario: Version consistency check passes
    Given a container named "test-bump-consistency-check-pass" is prepared
    And the version is defined as "1.0.0" in all version files
    When I run command in container "~/.local/bin/cz --no-raise 21 bump --yes --check-consistency"
    Then the output should contain
      | content                     |
      | bump: version 1.0.0 → 1.0.0 |
      | tag to create: 1.0.0        |

  @bump-consistency-check-fail
  Scenario: Version consistency check fails
    Given a container named "test-bump-consistency-check-fail" is prepared
    And there is a commit with "feat" in the message
    And the version is defined as "1.0.0" in some version files but not others
    When I run command in container "~/.local/bin/cz bump --yes --check-consistency" that should fail
    Then the output should contain
      | content                                                                                                  |
      | Current version 1.0.0 is not found in version_file2.py.                                                  |
      | The version defined in commitizen configuration and the ones in version_files are possibly inconsistent. |