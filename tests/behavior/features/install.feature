@install
Feature: Validate the 'install' Task Functionality
  In order to ensure reliable setup in containerized environments
  As a developer or system administrator
  I want to verify that the 'install' task completes successfully
  So that the necessary Commitizen and dependencies are correctly installed for development

  @install-success
  Scenario: Verifying 'install' task success
    Given a container named "test-install-success" is prepared
    When I run command in container "task commitizen:install"
    Then the output should contain
      | content                                                                               |
      | 👍 Commitizen and dependencies are successfully installed in the virtual environment  |
      | 👍 Commitizen is now configured. You can use 'cz version' directly from any location. |
    When I run command in container "source ~/.profile && cd / && cz version"
    Then the output should contain
      | content |
      | 3.13.0  |

  @install-up-to-date
  Scenario: Check Commitizen is up to date
    Given a container named "test-install-up-to-date" is prepared
    When I run command in container "task commitizen:install"
    And I run command in container "task commitizen:install"
    Then the output should contain
      | content |
      |         |
