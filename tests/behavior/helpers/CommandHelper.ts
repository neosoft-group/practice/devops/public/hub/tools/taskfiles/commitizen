import Helper from '@codeceptjs/helper';
import { execSync } from 'child_process';

class CommandHelper extends Helper {
    _init() {
        this.executeHostCommand(`docker build --tag commitizen-ubuntu:22.04 --file dockerfiles/Dockerfile.Ubuntu.22.04 ../../.`);
    }

    // 🌟 Executes commands on the host machine.
    // This function manages both successful executions and errors. 
    // Centralizing this handling allows for easier maintenance and provides a single point for modifications.
    async executeHostCommand(command: string, expectError: boolean = false) {
        try {
            const output = execSync(`${command}`, { stdio: 'pipe' });
            return output.toString();
        } catch (error) {
            if (expectError) {
                return error.toString();
            } else {
                console.error(`Error executing command: ${error.message}`);
                throw error;
            }
        }
    }

    // 📦 Simplifies executing Docker commands by wrapping them with the necessary Docker syntax.
    // This method uses `executeHostCommand` to ensure error handling consistency and reduces code duplication.
    async executeDockerCommand(container: string, command: string, expectError: boolean = false) {
        const fullCommand = `docker exec -w /workspace ${container} bash -c "${command}"`;
        return await this.executeHostCommand(fullCommand, expectError);
    };
}

export = CommandHelper;
