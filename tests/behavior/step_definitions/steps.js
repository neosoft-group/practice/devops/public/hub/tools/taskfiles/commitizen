const { I } = inject();

// A variable to store the execution result
let output = '';
let container = '';

const addCommitWithType = async (commitType) => {
  I.executeDockerCommand(container, `
      echo 'Test modification' >> very_fake_file.md &&
      git add very_fake_file.md &&
      git commit -m '${commitType}: Test modification'
  `);
};

const generateUniqueVersion = (currentVersion) => {
  let newVersion;
  do {
    const major = Math.floor(Math.random() * 3) + 1;
    const minor = Math.floor(Math.random() * 10);
    const patch = Math.floor(Math.random() * 10);
    newVersion = `${major}.${minor}.${patch}`;
  } while (newVersion === currentVersion);
  return newVersion;
};

Given('a container named {string} is prepared', async (containerName) => {
  container = containerName;
  await I.executeHostCommand(`docker rm -f ${container} || true`);
  await I.executeHostCommand(`docker run -d --name ${container} commitizen-ubuntu:22.04 sleep infinity`);
});
Given('I run command in container {string}', async (command) => {
  output = await I.executeDockerCommand(container, command);
});
Given('I run command in container {string} that should fail', async (command) => {
  output = await I.executeDockerCommand(container, `${command}`, expectError = true);
});
Given('there is a commit with {string} in the message', async (commitType) => {
  await addCommitWithType(commitType);
});
Given('the current version is an alpha prerelease', async () => {
  await addCommitWithType('feat');
  output = await I.executeDockerCommand(container, `~/.local/bin/cz bump --yes --prerelease alpha`);
});

Then("the output should contain", (table) => {
  for (const id in table.rows) {
    if (id < 1) {
      continue; // skip a header of a table
    }

    // go by row cells
    const cells = table.rows[id].cells;

    // take values
    const expectedOutput = cells[0].value;

    if (!output.includes(expectedOutput)) {
      const error_message = `\n` +
        `${output}\n\n` +
        `=> Expected to see exactly "${expectedOutput}"`;
      throw new Error(error_message);
    }
  }
});

Given('the version is defined as {string} in all version files', async (version) => {
  output = await I.executeDockerCommand(container, `echo '${version}' > version_file1.txt`);
  output = await I.executeDockerCommand(container, `echo '${version}' > version_file2.py`);
  output = await I.executeDockerCommand(container, `echo '${version}' > version_file3.json`);
});

Given('the version is defined as {string} in some version files but not others', async (version) => {
  output = await I.executeDockerCommand(container, `echo '${version}' > version_file1.txt`);
  output = await I.executeDockerCommand(container, `echo '${generateUniqueVersion(version)}' > version_file2.py`);
  output = await I.executeDockerCommand(container, `echo '${generateUniqueVersion(version)}' > version_file3.json`);
});

Given('the container is on the {string} branch', async (branch) => {
  output = await I.executeDockerCommand(container, `git checkout ${branch}`);
});

Given('there are uncommitted changes in the container', async () => {
  output = await I.executeDockerCommand(container, `echo 'Temporary change' > temp_file.txt && git add temp_file.txt`);
});
