/// <reference types='codeceptjs' />
type steps_file = typeof import('./steps_file');
type CommandHelper = import('./helpers/CommandHelper');

declare namespace CodeceptJS {
  interface SupportObject { I: I, current: any }
  interface Methods extends CommandHelper {}
  interface I extends ReturnType<steps_file>, WithTranslation<Methods> {}
  namespace Translation {
    interface Actions {}
  }
}
